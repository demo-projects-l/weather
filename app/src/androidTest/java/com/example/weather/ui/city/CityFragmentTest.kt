package com.example.weather.ui.city

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather.R
import com.example.weather.launchFragmentInHiltContainer
import com.example.weather.testUtil.MainCoroutineRule
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
@HiltAndroidTest
class CityFragmentTest {
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

//    @BindValue
//    @JvmField
//    val decksRepository: DecksRepository = Mockito.mock(DecksRepository::class.java)

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun show_fragment() = mainCoroutineRule.runBlockingTest {
        launchFragmentInHiltContainer<CityFragment>()

        onView(
            allOf(
                withId(R.id.temperature),
                withText("TextView")
            )
        )
            .check(matches(isDisplayed()))
    }
}