package com.example.weather.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather.data.source.local.WeatherDatabase
import com.example.weather.data.source.remote.WeatherApiServiceBuilder
import com.example.weather.data.source.remote.WeatherRemoteDataSourceImpl
import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather
import com.example.weather.testUtil.MockResponseFileReader
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.time.ZoneOffset
import com.example.weather.data.source.remote.mapper.WeatherMapper as RemoteMapper

@RunWith(AndroidJUnit4::class)
class DefaultWeatherRepositoryTest {
    private val mockWebServer = MockWebServer()
    private val apiService = WeatherApiServiceBuilder()
        .baseUrl(mockWebServer.url("/").toString())
        .apiKey("key")
        .build()

    private val stoppedClock = Clock.tick(
        Clock.fixed(Instant.now(), ZoneOffset.UTC),
        Duration.ofSeconds(1)
    )

    private lateinit var database: WeatherDatabase

    private lateinit var repository: DefaultWeatherRepository

    @Before
    fun setUpRepository() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            WeatherDatabase::class.java
        ).allowMainThreadQueries().build()

        val remoteDataSource = WeatherRemoteDataSourceImpl(
            apiService,
            RemoteMapper(stoppedClock)
        )

        repository = DefaultWeatherRepository(remoteDataSource)
    }

    @After
    fun closeDb() = database.close()

    @Test
    fun getCityWeather() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setBody(MockResponseFileReader("success_response.json").content)
        )

        assertThat(repository.getCityWeather("Kaunas")).isEqualTo( // TODO?
            Result.Success(
                Weather(
                    "Kaunas",
                    12.0,
                    "clear sky",
                    "01d",
                    Instant.ofEpochSecond(1604685600),
                    stoppedClock.instant(),
                    598316
                )
            )
        )

        return@runBlocking
    }

    @Test
    fun getCityListWeather() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(MockResponseFileReader("list_of_cities_weather_response.json").content)
        )

        val result = repository.getForCities(listOf(593116, 596238, 2750405, 4887398))

        assertThat((result as Result.Success).data).containsExactly(
            // TODO?
            Weather(
                "Vilnius",
                4.13,
                "overcast clouds",
                "04d",
                Instant.ofEpochSecond(1605204000),
                stoppedClock.instant(),
                593116
            ),
            Weather(
                "Palanga",
                5.0,
                "overcast clouds",
                "04d",
                Instant.ofEpochSecond(1605204000),
                stoppedClock.instant(),
                596238
            ),
            Weather(
                "Netherlands",
                11.64,
                "broken clouds",
                "04d",
                Instant.ofEpochSecond(1605204000),
                stoppedClock.instant(),
                2750405
            ),
            Weather(
                "Chicago",
                3.38,
                "scattered clouds",
                "03n",
                Instant.ofEpochSecond(1605204000),
                stoppedClock.instant(),
                4887398
            ),
        )

        return@runBlocking
    }
}