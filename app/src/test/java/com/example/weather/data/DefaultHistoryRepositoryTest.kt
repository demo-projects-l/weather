package com.example.weather.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather.data.source.local.HistoryLocalDataSourceImpl
import com.example.weather.data.source.local.WeatherDatabase
import com.example.weather.data.source.local.mapper.WeatherMapper
import com.example.weather.domain.model.Weather
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.google.common.truth.Truth.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.Instant
import java.time.temporal.ChronoUnit

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class DefaultHistoryRepositoryTest {
    // Set the main coroutines dispatcher for unit testing.
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: WeatherDatabase
    private lateinit var repository: DefaultHistoryRepository

    @Before
    fun setUpRepository() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            WeatherDatabase::class.java
        ).allowMainThreadQueries().build()

        val localDataSource = HistoryLocalDataSourceImpl(database.weatherDao(), WeatherMapper())

        repository = DefaultHistoryRepository(localDataSource)
    }

    @After
    fun closeDb() = database.close()

    @Test
    fun save() = mainCoroutineRule.runBlockingTest {
        val now = Instant.now().let {
            it.minusNanos(it.nano.toLong())
        }

        // WHEN
        repository.save(
            Weather("Vilnius", 11.12, "Snowing", "xvc", now, now, 34)
        )

        val historyData = repository.observe()

        // THEN
        historyData.observeForTesting {
            assertThat(historyData.getOrAwaitValue())
                .containsExactly(
                    Weather("Vilnius", 11.12, "Snowing", "xvc", now, now, 34, 1)
                )
        }
    }

    @Test
    fun update() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now().let {
            it.minusNanos(it.nano.toLong())
        }
        repository.save(
            Weather(
                "Vilnius",
                11.12,
                "Snowing",
                "xvc",
                now.minus(2, ChronoUnit.DAYS),
                now.minus(2, ChronoUnit.DAYS),
                34
            )
        )


        // WHEN
        repository.update(
            1,
            Weather("Vilnius", 2.2, "Gloomy", "eer", now, now, 34)
        )

        val historyData = repository.observe()

        // THEN
        historyData.observeForTesting {
            assertThat(historyData.getOrAwaitValue()).containsExactly(
                Weather("Vilnius", 2.2, "Gloomy", "eer", now, now, 34, 1)
            )
        }
    }

    @Test
    fun getCity() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now().let {
            it.minusNanos(it.nano.toLong())
        }

        repository.save(Weather("Vilnius", 11.12, "Snowing", "xvc", now, now, 34))
        repository.save(Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21))

        // WHEN
        val city = repository.getCity("kaunas") // TODO case insensitive

        // THEN
        assertThat(city).isEqualTo(Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21, 2))
    }

    @Test
    fun orderHistoryByViewTimeDesc() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now().let {
            it.minusNanos(it.nano.toLong())
        }

        repository.save(
            Weather(
                "Vilnius",
                11.12,
                "Snowing",
                "xvc",
                now.minus(2, ChronoUnit.DAYS),
                now.minus(2, ChronoUnit.DAYS),
                34
            )
        )
        repository.save(Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21))
        repository.save(
            Weather(
                "Klaipeda",
                3.91,
                "Windy",
                "wg",
                now.minus(1, ChronoUnit.DAYS),
                now.minus(1, ChronoUnit.DAYS),
                23
            )
        )

        // WHEN
        val history = repository.observe()

        // THEN
        history.observeForTesting {
            assertThat(history.getOrAwaitValue())
                .containsExactly(
                    Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21, 2),
                    Weather(
                        "Klaipeda",
                        3.91,
                        "Windy",
                        "wg",
                        now.minus(1, ChronoUnit.DAYS),
                        now.minus(1, ChronoUnit.DAYS),
                        23,
                        3
                    ),
                    Weather(
                        "Vilnius",
                        11.12,
                        "Snowing",
                        "xvc",
                        now.minus(2, ChronoUnit.DAYS),
                        now.minus(2, ChronoUnit.DAYS),
                        34,
                        1
                    )
                ).inOrder()
        }
    }

    @Test
    fun getAllHistory() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now().let {
            it.minusNanos(it.nano.toLong())
        }

        repository.save(
            Weather(
                "Vilnius",
                11.12,
                "Snowing",
                "xvc",
                now.minus(2, ChronoUnit.DAYS),
                now.minus(2, ChronoUnit.DAYS),
                34
            )
        )
        repository.save(Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21))
        repository.save(
            Weather(
                "Klaipeda",
                3.91,
                "Windy",
                "wg",
                now.minus(1, ChronoUnit.DAYS),
                now.minus(1, ChronoUnit.DAYS),
                23
            )
        )

        // WHEN
        val history = repository.getAll()

        // THEN
        assertThat(history)
            .containsExactly(
                Weather("Kaunas", 9.11, "Storm", "ttt", now, now, 21, 2),
                Weather(
                    "Klaipeda",
                    3.91,
                    "Windy",
                    "wg",
                    now.minus(1, ChronoUnit.DAYS),
                    now.minus(1, ChronoUnit.DAYS),
                    23,
                    3
                ),
                Weather(
                    "Vilnius",
                    11.12,
                    "Snowing",
                    "xvc",
                    now.minus(2, ChronoUnit.DAYS),
                    now.minus(2, ChronoUnit.DAYS),
                    34,
                    1
                )
            )
    }
}