package com.example.weather.data.source.remote

import com.example.weather.data.source.remote.mapper.WeatherMapper
import com.example.weather.domain.Result
import com.example.weather.domain.Result.Success
import com.example.weather.domain.exception.BadInputException
import com.example.weather.domain.exception.UnauthorizedException
import com.example.weather.domain.model.Weather
import com.example.weather.testUtil.MockResponseFileReader
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset

class WeatherRemoteDataSourceTest {
    private val mockWebServer = MockWebServer()
    private val apiService = WeatherApiServiceBuilder()
        .baseUrl(mockWebServer.url("/").toString())
        .apiKey("key")
        .build()

    private val now: Instant = Instant.now()

    private val remoteDataSource = WeatherRemoteDataSourceImpl(
        apiService,
        WeatherMapper(Clock.fixed(now, ZoneOffset.UTC))
    )

    @Test
    fun getCityWeather() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setBody(MockResponseFileReader("success_response.json").content)
        )

        assertThat(remoteDataSource.getCityWeather("Kaunas")).isEqualTo(
            Success(
                Weather(
                    "Kaunas",
                    12.0,
                    "clear sky",
                    "01d",
                    Instant.ofEpochSecond(1604685600),
                    now,
                    598316
                )
            )
        )

        return@runBlocking
    }

    @Test
    fun returnResultErrorWhenHostIsNotResolved() = runBlocking {
        val apiService = WeatherApiServiceBuilder()
            .baseUrl("http://invalid/URL/")
            .apiKey("key")
            .build()


        val remoteDataSource =
            WeatherRemoteDataSourceImpl(apiService, WeatherMapper(Clock.fixed(now, ZoneOffset.UTC)))

        assertThat(remoteDataSource.getCityWeather("Prienai"))
            .isInstanceOf(Result.Error::class.java)

        return@runBlocking
    }

    @Test
    fun returnResultErrorWhenApiKeyExpires() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(MockResponseFileReader("invalid_key_response.json").content)
                .setResponseCode(401)
        )

        val result = remoteDataSource.getCityWeather("Prienai") as Result.Error

        assertThat(result.exception).isInstanceOf(UnauthorizedException::class.java)

        return@runBlocking
    }

    @Test
    fun returnResultErrorWhenCityIsNotFound() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(MockResponseFileReader("city_not_found_response.json").content)
                .setResponseCode(404)
        )

        val result = remoteDataSource.getCityWeather("Prienai") as Result.Error

        assertThat(result.exception).isInstanceOf(BadInputException::class.java)

        return@runBlocking
    }

    @Test
    fun returnResultErrorOnServerError() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody("")
                .setResponseCode(500)
        )

        assertThat(remoteDataSource.getCityWeather("Prienai"))
            .isInstanceOf(Result.Error::class.java)

        return@runBlocking
    }

    @Test
    fun getBatch() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(MockResponseFileReader("list_of_cities_weather_response.json").content)
        )

        val result = remoteDataSource.getForCities(listOf(593116, 596238, 2750405, 4887398))

        assertThat((result as Success).data)
            .containsExactly(
                Weather(
                    "Vilnius",
                    4.13,
                    "overcast clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now,
                    593116
                ),
                Weather(
                    "Palanga",
                    5.0,
                    "overcast clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now,
                    596238
                ),
                Weather(
                    "Netherlands",
                    11.64,
                    "broken clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now,
                    2750405
                ),
                Weather(
                    "Chicago",
                    3.38,
                    "scattered clouds",
                    "03n",
                    Instant.ofEpochSecond(1605204000),
                    now,
                    4887398
                ),
            )

        return@runBlocking
    }
}