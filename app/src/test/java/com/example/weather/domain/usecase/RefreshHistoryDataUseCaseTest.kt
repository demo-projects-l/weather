package com.example.weather.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather.data.DefaultHistoryRepository
import com.example.weather.data.DefaultWeatherRepository
import com.example.weather.data.source.local.HistoryLocalDataSourceImpl
import com.example.weather.data.source.local.WeatherDatabase
import com.example.weather.data.source.local.mapper.WeatherMapper
import com.example.weather.data.source.remote.WeatherApiServiceBuilder
import com.example.weather.data.source.remote.WeatherRemoteDataSourceImpl
import com.example.weather.domain.model.Weather
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.MockResponseFileReader
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

@RunWith(AndroidJUnit4::class)
class RefreshHistoryDataUseCaseTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: WeatherDatabase
    private lateinit var historyRepository: DefaultHistoryRepository

    private val mockWebServer = MockWebServer()
    private val apiService = WeatherApiServiceBuilder()
        .baseUrl(mockWebServer.url("/").toString())
        .apiKey("key")
        .build()

    private val now: Instant = Instant.now().let { it.minusNanos(it.nano.toLong()) }

    private val remoteDataSource = WeatherRemoteDataSourceImpl(
        apiService,
        com.example.weather.data.source.remote.mapper.WeatherMapper(
            Clock.fixed(
                now,
                ZoneOffset.UTC
            )
        )
    )

    private val weatherRepository = DefaultWeatherRepository(remoteDataSource)

    @Before
    fun setUpRepository() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            WeatherDatabase::class.java
        ).allowMainThreadQueries().build()

        val localDataSource = HistoryLocalDataSourceImpl(database.weatherDao(), WeatherMapper())

        historyRepository = DefaultHistoryRepository(localDataSource)
    }

    @After
    fun closeDb() = database.close()

    @Test
    fun refreshHistory() = runBlocking {
        // GIVEN
        mockWebServer.enqueue(
            MockResponse().setBody(
                MockResponseFileReader("list_of_cities_weather_response.json").content
            )
        )

        val useCase = RefreshHistoryDataUseCase(weatherRepository, historyRepository)

        historyRepository.save(
            Weather(
                "Vilnius",
                1.1,
                "dark clouds",
                "11a",
                Instant.ofEpochSecond(1601104000),
                now.minus(7, ChronoUnit.DAYS),
                593116
            )
        )

        historyRepository.save(
            Weather(
                "Palanga",
                1.2,
                "sunny clouds",
                "12a",
                Instant.ofEpochSecond(1601104000),
                now.minus(2, ChronoUnit.DAYS),
                596238
            )
        )

        historyRepository.save(
            Weather(
                "Netherlands",
                1.3,
                "sad clouds",
                "13a",
                Instant.ofEpochSecond(1601104000),
                now.minus(3, ChronoUnit.DAYS),
                2750405
            )
        )

        historyRepository.save(
            Weather(
                "Chicago",
                1.4,
                "some clouds",
                "14a",
                Instant.ofEpochSecond(1601104000),
                now.minus(4, ChronoUnit.DAYS),
                4887398
            )
        )

        // WHEN
        val history = historyRepository.observe()
        useCase()

        // THEN
        history.observeForTesting {
            assertThat(history.getOrAwaitValue()).containsExactly(
                Weather(
                    "Vilnius",
                    4.13,
                    "overcast clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now.minus(7, ChronoUnit.DAYS),
                    593116,
                    1
                ),
                Weather(
                    "Palanga",
                    5.0,
                    "overcast clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now.minus(2, ChronoUnit.DAYS),
                    596238,
                    2
                ),
                Weather(
                    "Netherlands",
                    11.64,
                    "broken clouds",
                    "04d",
                    Instant.ofEpochSecond(1605204000),
                    now.minus(3, ChronoUnit.DAYS),
                    2750405,
                    3
                ),
                Weather(
                    "Chicago",
                    3.38,
                    "scattered clouds",
                    "03n",
                    Instant.ofEpochSecond(1605204000),
                    now.minus(4, ChronoUnit.DAYS),
                    4887398,
                    4
                ),
            )
        }

        return@runBlocking
    }
}