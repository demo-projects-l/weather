package com.example.weather.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.google.common.truth.Truth.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import java.time.Instant

@ExperimentalCoroutinesApi
class ObserveHistoryUseCaseTest {
    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun getHistory() = mainCoroutineRule.runBlockingTest {
        val now = Instant.now()
        val historyData = listOf(
            Weather("Vilnius", 9.8, "Dry", "D", now, now, 101),
            Weather("Kaunas", 3.69, "Broken clouds", "B", now, now, 102),
            Weather("Palanga", 9.99, "Sunny", "S", now, now, 103),
        )

        val historyRepositoryMock = mock(HistoryRepository::class.java)
        `when`(historyRepositoryMock.observe()).thenReturn(liveData { emit(historyData) })

        val getHistoryUseCase = ObserveHistoryUseCase(historyRepositoryMock)

        val result = getHistoryUseCase()

        result.observeForTesting {
            assertThat(result.getOrAwaitValue()).isEqualTo(Result.Success(historyData))
        }
    }
}