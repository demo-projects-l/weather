package com.example.weather.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.WeatherRepository
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import com.example.weather.domain.Result
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.testUtil.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.mockito.Mockito.*
import java.time.Instant
import com.example.weather.testUtil.any
import java.time.temporal.ChronoUnit

@ExperimentalCoroutinesApi
class FindCityUseCaseTest {
    // Set the main coroutines dispatcher for unit testing.
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun getCityTemp_saveHistory() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now()
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        `when`(weatherRepositoryMock.getCityWeather("Vilnius"))
            .thenReturn(Result.Success(Weather("Vilnius", 12.5, "Rainy", "icon", now, now, 101)))

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val findCityUseCase = FindCityUseCase(weatherRepositoryMock, historyRepositoryMock)

        // WHEN
        val result = findCityUseCase("Vilnius")

        // THEN
        assertThat(result)
            .isEqualTo(
                Result.Success(Weather("Vilnius", 12.5, "Rainy", "icon", now, now, 101))
            )

        verify(historyRepositoryMock).save(Weather("Vilnius", 12.5, "Rainy", "icon", now, now, 101))
    }

    @Test
    fun getCityTemp_handleFailure() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val exception = RuntimeException("Request failure")
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        `when`(weatherRepositoryMock.getCityWeather("Vilnius"))
            .thenReturn(Result.Error(exception))

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val findCityUseCase = FindCityUseCase(weatherRepositoryMock, historyRepositoryMock)

        // WHEN
        val result = findCityUseCase("Vilnius")

        // THEN
        assertThat(result).isEqualTo(Result.Error(exception))
        verify(historyRepositoryMock, never()).save(any(Weather::class.java))
    }

    @Test
    fun updateHistoryIfCityExists() = mainCoroutineRule.runBlockingTest {
        // GIVEN
        val now = Instant.now()
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        `when`(weatherRepositoryMock.getCityWeather("Vilnius"))
            .thenReturn(Result.Success(Weather("Vilnius", 4.2, "Sunny", "another", now, now, 101)))

        val historyRepositoryMock = mock(HistoryRepository::class.java)
        `when`(historyRepositoryMock.getCity("Vilnius"))
            .thenReturn(
                Weather(
                    "Vilnius",
                    12.5,
                    "Rainy",
                    "icon",
                    now.minus(2, ChronoUnit.DAYS),
                    now.minus(2, ChronoUnit.DAYS),
                    101,
                    7
                )
            )

        val findCityUseCase = FindCityUseCase(weatherRepositoryMock, historyRepositoryMock)

        // WHEN
        val result = findCityUseCase("Vilnius")

        // THEN
        assertThat(result)
            .isEqualTo(
                Result.Success(Weather("Vilnius", 4.2, "Sunny", "another", now, now, 101))
            )

        verify(historyRepositoryMock).update(
            7,
            Weather(
                "Vilnius",
                4.2,
                "Sunny",
                "another",
                now,
                now,
                101
            )
        )
    }
}