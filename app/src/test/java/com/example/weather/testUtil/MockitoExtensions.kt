package com.example.weather.testUtil

import org.mockito.Mockito

// Hack fot Kotlin
fun <T> any(type: Class<T>): T = Mockito.any<T>(type)