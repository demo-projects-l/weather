package com.example.weather.ui.city

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.weather.domain.Result
import com.example.weather.domain.exception.UnauthorizedException
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.domain.usecase.FindCityUseCase
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.any
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.example.weather.ui.ErrorTranslator
import com.google.common.truth.Truth.*
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import java.time.Instant

@ExperimentalCoroutinesApi
class CityViewModelTest {
    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun indicateDataLoading() {
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        val errorTranslatorMock = mock(ErrorTranslator::class.java)

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val viewModel = CityViewModel(
            FindCityUseCase(weatherRepositoryMock, historyRepositoryMock),
            errorTranslatorMock
        )

        viewModel.dataLoading.observeForTesting {
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(true)
        }
    }

    @Test
    fun loadWeatherData() = mainCoroutineRule.runBlockingTest {
        val date = Instant.ofEpochSecond(1604246400) // 2020-11-01 18:00:00
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        `when`(weatherRepositoryMock.getCityWeather("Kaunas"))
            .thenReturn(
                Result.Success(
                    Weather(
                        "Vilnius",
                        12.0,
                        "Rainy",
                        "icon",
                        date,
                        Instant.now(),
                        101
                    )
                )
            )

        val errorTranslatorMock = mock(ErrorTranslator::class.java)

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val viewModel = CityViewModel(
            FindCityUseCase(weatherRepositoryMock, historyRepositoryMock),
            errorTranslatorMock
        )

        viewModel.findCity("Kaunas")

        viewModel.dataLoading.observeForTesting {
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(false)
        }

        viewModel.city.observeForTesting {
            assertThat(viewModel.city.getOrAwaitValue()).isEqualTo("Vilnius")
        }

        viewModel.temperature.observeForTesting {
            assertThat(viewModel.temperature.getOrAwaitValue()).isEqualTo(12.0)
        }

        viewModel.description.observeForTesting {
            assertThat(viewModel.description.getOrAwaitValue()).isEqualTo("Rainy")
        }

        viewModel.icon.observeForTesting {
            assertThat(viewModel.icon.getOrAwaitValue()).isEqualTo("icon")
        }

        viewModel.measuredAt.observeForTesting {
            assertThat(viewModel.measuredAt.getOrAwaitValue()).isEqualTo(date)
        }

        viewModel.error.observeForTesting {
            assertThat(viewModel.error.getOrAwaitValue()).isEqualTo(null)
        }
    }

    @Test
    fun loadWeatherData_indicateLoading() = mainCoroutineRule.runBlockingTest {
        val date = Instant.ofEpochSecond(1604246400) // 2020-11-01 18:00:00
        val weatherRepositoryMock = mockk<WeatherRepository>() // TODO move to set up

        coEvery { weatherRepositoryMock.getCityWeather("Kaunas") } coAnswers {
            delay(1000)
            Result.Success(Weather("Vilnius", 12.5, "Rainy", "icon", date, Instant.now(), 101))
        }

        val errorTranslatorMock = mock(ErrorTranslator::class.java)

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val viewModel = CityViewModel(
            FindCityUseCase(weatherRepositoryMock, historyRepositoryMock),
            errorTranslatorMock
        )

        mainCoroutineRule.pauseDispatcher()

        viewModel.findCity("Kaunas")

        viewModel.dataLoading.observeForTesting {
            mainCoroutineRule.runCurrent()
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(true)
            assertThat(viewModel.city.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.temperature.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.description.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.icon.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.measuredAt.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.error.getOrAwaitValue()).isEqualTo(null)

            mainCoroutineRule.resumeDispatcher()

            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(false)
            assertThat(viewModel.city.getOrAwaitValue()).isEqualTo("Vilnius")
            assertThat(viewModel.temperature.getOrAwaitValue()).isEqualTo(12.5)
            assertThat(viewModel.description.getOrAwaitValue()).isEqualTo("Rainy")
            assertThat(viewModel.icon.getOrAwaitValue()).isEqualTo("icon")
            assertThat(viewModel.measuredAt.getOrAwaitValue()).isEqualTo(date)
            assertThat(viewModel.error.getOrAwaitValue()).isEqualTo(null)
        }
    }

    @Test
    fun translateError() = mainCoroutineRule.runBlockingTest {
        val weatherRepositoryMock = mockk<WeatherRepository>()

        coEvery { weatherRepositoryMock.getCityWeather("Kaunas") } coAnswers {
            delay(1000)
            Result.Error(UnauthorizedException("Token"))
        }

        val errorTranslatorMock = mock(ErrorTranslator::class.java)
        `when`(errorTranslatorMock.translate(any(UnauthorizedException::class.java)))
            .thenReturn("Token error")

        val historyRepositoryMock = mock(HistoryRepository::class.java)

        val viewModel = CityViewModel(
            FindCityUseCase(weatherRepositoryMock, historyRepositoryMock),
            errorTranslatorMock
        )

        mainCoroutineRule.pauseDispatcher()

        viewModel.findCity("Kaunas")

        viewModel.dataLoading.observeForTesting {
            mainCoroutineRule.runCurrent()
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(true)
            assertThat(viewModel.city.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.temperature.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.description.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.icon.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.measuredAt.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.error.getOrAwaitValue()).isEqualTo(null)

            mainCoroutineRule.resumeDispatcher()

            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(false)
            assertThat(viewModel.city.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.temperature.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.description.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.icon.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.measuredAt.getOrAwaitValue()).isEqualTo(null)
            assertThat(viewModel.error.getOrAwaitValue()).isEqualTo("Token error")
        }
    }
}