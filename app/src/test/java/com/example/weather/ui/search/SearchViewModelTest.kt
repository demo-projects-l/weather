package com.example.weather.ui.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class SearchViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun triggerNavigationToCity() = mainCoroutineRule.runBlockingTest {
        val viewModel = SearchViewModel()

        viewModel.navigateToCity.observeForTesting {
            viewModel.findCity("Vilnius")
            Truth.assertThat(
                viewModel.navigateToCity.getOrAwaitValue().getContentIfNotHandled()
            ).isEqualTo("Vilnius")
        }
    }

    @Test
    fun preventDoubleEventHandling() = mainCoroutineRule.runBlockingTest { // TODO move to event test
        val viewModel = SearchViewModel()

        viewModel.navigateToCity.observeForTesting {
            viewModel.findCity("Vilnius")
            Truth.assertThat(
                viewModel.navigateToCity.getOrAwaitValue().getContentIfNotHandled()
            ).isEqualTo("Vilnius")

            Truth.assertThat(
                viewModel.navigateToCity.getOrAwaitValue().getContentIfNotHandled()
            ).isEqualTo(null)
        }
    }

    @Test
    fun triggerNavigationToHistory() = mainCoroutineRule.runBlockingTest {
        val viewModel = SearchViewModel()

        viewModel.navigateToHistory.observeForTesting {
            viewModel.goToHistory()
            Truth.assertThat(
                viewModel.navigateToHistory.getOrAwaitValue().getContentIfNotHandled()
            ).isEqualTo(true)
        }
    }
}