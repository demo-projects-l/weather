package com.example.weather.ui.history

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.domain.usecase.ObserveHistoryUseCase
import com.example.weather.domain.usecase.RefreshHistoryDataUseCase
import com.example.weather.testUtil.MainCoroutineRule
import com.example.weather.testUtil.getOrAwaitValue
import com.example.weather.testUtil.observeForTesting
import com.google.common.truth.Truth.*
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import java.time.Instant

@ExperimentalCoroutinesApi
class HistoryViewModelTest {
    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun indicateDataLoading() = mainCoroutineRule.runBlockingTest {
        val historyRepositoryMock = mockk<HistoryRepository>()
        val weatherRepositoryMock = mockk<WeatherRepository>()
        val viewModel = HistoryViewModel(
            ObserveHistoryUseCase(historyRepositoryMock),
            RefreshHistoryDataUseCase(weatherRepositoryMock, historyRepositoryMock)
        )

        coEvery { historyRepositoryMock.observe() } coAnswers {
            delay(1000)
            liveData { emptyList<Weather>() }
        }

        coEvery { historyRepositoryMock.getAll() } coAnswers {
            emptyList()
        }

        mainCoroutineRule.pauseDispatcher()

        viewModel.dataLoading.observeForTesting {
            mainCoroutineRule.runCurrent()
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(true)
        }
    }

    @Test
    fun loadHistoryData() = mainCoroutineRule.runBlockingTest {
        val date = Instant.ofEpochSecond(1604246400) // 2020-11-01 18:00:00
        val now = Instant.now()
        val historyData = listOf(
            Weather("Vilnius", 9.8, "Dry", "D", date, now, 101, 1),
            Weather("Kaunas", 3.69, "Broken clouds", "B", date, now, 102, 2),
            Weather("Palanga", 9.99, "Sunny", "S", date, now, 103, 3),
        )

        val historyRepositoryMock = mock(HistoryRepository::class.java)
        `when`(historyRepositoryMock.observe()).thenReturn(liveData { emit(historyData) })
        `when`(historyRepositoryMock.getAll()).thenReturn(historyData)

        val weatherRepositoryMock = mock(WeatherRepository::class.java)

        val viewModel = HistoryViewModel(
            ObserveHistoryUseCase(historyRepositoryMock),
            RefreshHistoryDataUseCase(weatherRepositoryMock, historyRepositoryMock)
        )

        viewModel.dataLoading.observeForTesting {
            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(false)
        }

        viewModel.history.observeForTesting {
            assertThat(viewModel.history.getOrAwaitValue()).isEqualTo(historyData)
        }
    }

    @Test
    fun loadHistoryData_indicateLoading() = mainCoroutineRule.runBlockingTest {
        val date = Instant.ofEpochSecond(1604246400) // 2020-11-01 18:00:00
        val now = Instant.now()
        val historyData = listOf(
            Weather("Vilnius", 9.8, "Dry", "D", date, now, 101, 1),
            Weather("Kaunas", 3.69, "Broken clouds", "B", date, now, 101, 2),
            Weather("Palanga", 9.99, "Sunny", "S", date, now, 101, 3),
        )

        val historyRepositoryMock = mockk<HistoryRepository>()
        val weatherRepositoryMock = mock(WeatherRepository::class.java)
        val viewModel = HistoryViewModel(
            ObserveHistoryUseCase(historyRepositoryMock),
            RefreshHistoryDataUseCase(weatherRepositoryMock, historyRepositoryMock),
        )

        coEvery { historyRepositoryMock.observe() } coAnswers {
            delay(1000)
            liveData { emit(historyData) }
        }

        coEvery { historyRepositoryMock.getAll() } coAnswers {
            historyData
        }

        mainCoroutineRule.pauseDispatcher()

        viewModel.dataLoading.observeForTesting {
            mainCoroutineRule.runCurrent()

            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(true)
            assertThat(viewModel.history.getOrAwaitValue()).isEqualTo(emptyList<Weather>())

            mainCoroutineRule.resumeDispatcher()

            assertThat(viewModel.dataLoading.getOrAwaitValue()).isEqualTo(false)
            assertThat(viewModel.history.getOrAwaitValue()).isEqualTo(historyData)
        }
    }
}