package com.example.weather.domain.exception

class NetworkException(message: String?) : RuntimeException(message) {
}