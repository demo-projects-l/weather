package com.example.weather.domain.model

import java.time.Instant

data class Weather(
    val cityName: String,
    val temperature: Double,
    val description: String,
    val icon: String,
    val measuredAt: Instant,
    val viewedAt: Instant,
    val remoteId: Long,
    val historyId: Long? = null,
)