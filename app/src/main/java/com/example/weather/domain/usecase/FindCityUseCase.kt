package com.example.weather.domain.usecase

import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.domain.Result
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.domain.succeeded
import javax.inject.Inject

class FindCityUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val historyRepository: HistoryRepository
) {
    suspend operator fun invoke(name: String): Result<Weather> {
        val result = weatherRepository.getCityWeather(name)

        if (result.succeeded) {
            val historyEntry = historyRepository.getCity(name)
            val cityWeather = (result as Result.Success).data

            if (historyEntry != null) {
                historyRepository.update(historyEntry.historyId!!, cityWeather)
            } else {
                historyRepository.save(cityWeather)
            }
        }

        return result
    }
}