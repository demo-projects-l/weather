package com.example.weather.domain.exception

class UnauthorizedException(message: String) : RuntimeException(message)