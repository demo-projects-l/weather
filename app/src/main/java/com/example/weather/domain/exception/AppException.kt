package com.example.weather.domain.exception

class AppException(message: String, val translation: String) : RuntimeException(message) {
}