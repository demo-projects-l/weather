package com.example.weather.domain.usecase

import com.example.weather.domain.Result
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.domain.succeeded
import javax.inject.Inject

class RefreshHistoryDataUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val historyRepository: HistoryRepository,
) {
    suspend operator fun invoke() {
        val remoteIds = historyRepository.getAll().associateBy { it.remoteId }
        if (remoteIds.isEmpty()) {
            return
        }

        val result = weatherRepository.getForCities(remoteIds.keys.toList())
        if (result.succeeded) {
            (result as Result.Success).data.forEach { newWeather ->
                val historyWeather =
                    remoteIds[newWeather.remoteId] ?: error("History entry missing")

                historyRepository.update(
                    historyWeather.historyId ?: error("History id missing"),
                    newWeather.copy(viewedAt = historyWeather.viewedAt)
                ) // TODO handle
            }
        }
    }
}