package com.example.weather.domain.repository

import androidx.lifecycle.LiveData
import com.example.weather.domain.model.Weather

interface HistoryRepository {
    suspend fun save(weather: Weather)
    suspend fun update(historyId: Long, weather: Weather)
    suspend fun observe(): LiveData<List<Weather>>
    suspend fun getCity(name: String): Weather?
    suspend fun getAll(): List<Weather>
}