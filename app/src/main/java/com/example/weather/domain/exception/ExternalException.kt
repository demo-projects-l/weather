package com.example.weather.domain.exception

class ExternalException(message: String) : RuntimeException(message)