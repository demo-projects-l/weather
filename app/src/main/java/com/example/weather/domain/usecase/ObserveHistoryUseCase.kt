package com.example.weather.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.HistoryRepository
import javax.inject.Inject

class ObserveHistoryUseCase @Inject constructor(private val historyRepository: HistoryRepository) {
    suspend operator fun invoke(): LiveData<Result<List<Weather>>> {
        return historyRepository.observe().map { Result.Success(it) }
    }
}