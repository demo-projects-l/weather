package com.example.weather.domain.exception

class BadInputException(message: String) : RuntimeException(message)