package com.example.weather.domain.repository

import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather

interface WeatherRepository {
    suspend fun getCityWeather(cityName: String): Result<Weather>
    suspend fun getForCities(ids: List<Long>): Result<List<Weather>>
}