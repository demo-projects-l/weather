package com.example.weather.domain.exception

class DataException(message: String) : RuntimeException(message)