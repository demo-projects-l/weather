package com.example.weather.ui.history

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.domain.model.Weather

@BindingAdapter("app:items")
fun RecyclerView.setItems(items: List<Weather>?) {
    items?.let {
        (adapter as HistoryAdapter).submitList(items)
    }
}