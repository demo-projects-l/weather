package com.example.weather.ui.city

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.weather.databinding.FragmentCityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CityFragment : Fragment() {
    private val viewModel by viewModels<CityViewModel>()
    private val args: CityFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewDataBinding = FragmentCityBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@CityFragment.viewModel
            }

        viewDataBinding.backButton.setOnClickListener { // TODO to view model?
            findNavController().popBackStack()
        }

        viewModel.findCity(args.city)

        return viewDataBinding.root
        // http://api.openweathermap.org/data/2.5/weather?q=kaunas,LTU&units=metric&appid=7587eaff3affbf8e56a81da4d6c51d06
    }
}