package com.example.weather.ui.city

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.weather.domain.model.Weather
import com.example.weather.domain.usecase.FindCityUseCase
import com.example.weather.domain.Result
import com.example.weather.domain.Result.Loading
import com.example.weather.domain.Result.Success
import com.example.weather.domain.succeeded
import com.example.weather.ui.ErrorTranslator
import kotlinx.coroutines.launch

class CityViewModel @ViewModelInject constructor(
    private val findCityUseCase: FindCityUseCase,
    private val errorTranslator: ErrorTranslator,
) : ViewModel() {
    private val _weather = MutableLiveData<Result<Weather>>(Loading)

    val dataLoading = _weather.map {
        it is Loading
    }

    val dataLoadedSuccessfully = _weather.map {
        it.succeeded
    }

    val city = _weather.map {
        if (it.succeeded) (it as Success).data.cityName else null
    }

    val temperature = _weather.map {
        if (it.succeeded) (it as Success).data.temperature else null
    }

    val description = _weather.map {
        if (it.succeeded) (it as Success).data.description else null
    }

    val icon = _weather.map {
        if (it.succeeded) (it as Success).data.icon else null
    }

    val measuredAt = _weather.map {
        if (it.succeeded) (it as Success).data.measuredAt else null
    }

    val error = _weather.map {
        if (it is Result.Error) errorTranslator.translate(it.exception) else null
    }

    fun findCity(cityName: String) {
        viewModelScope.launch {
            _weather.value = findCityUseCase(cityName)
        }
    }
}