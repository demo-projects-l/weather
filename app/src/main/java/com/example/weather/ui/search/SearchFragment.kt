package com.example.weather.ui.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.weather.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment() {
    private val viewModel by viewModels<SearchViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewDataBinding = FragmentSearchBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@SearchFragment.viewModel

                cityInput.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        this@SearchFragment.viewModel.findCity(cityInput.text.toString())
                    }

                    return@setOnEditorActionListener true
                }

                cityInput.setText("")
            }

        viewModel.navigateToCity.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let { cityName ->
                val action = SearchFragmentDirections.actionSearchFragmentToCityFragment(cityName)

                findNavController().navigate(action)
            }
        }

        viewModel.navigateToHistory.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let {
                val action = SearchFragmentDirections.actionSearchFragmentToHistoryFragment()

                findNavController().navigate(action)
            }
        }

        return viewDataBinding.root
    }
}