package com.example.weather.ui

interface ErrorTranslator {
    fun translate(exception: Exception): String
}