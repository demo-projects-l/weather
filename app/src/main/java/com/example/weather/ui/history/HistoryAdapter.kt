package com.example.weather.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.databinding.ListitemHistoryBinding
import com.example.weather.domain.model.Weather

class HistoryAdapter : ListAdapter<Weather, HistoryAdapter.ViewHolder>(WeatherDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(private val viewDataBinding: ListitemHistoryBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {

        fun bind(weather: Weather) {
            viewDataBinding.weather = weather
            viewDataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val viewDataBinding = ListitemHistoryBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(viewDataBinding)
            }
        }
    }
}

class WeatherDiffCallback : DiffUtil.ItemCallback<Weather>() {
    override fun areItemsTheSame(oldItem: Weather, newItem: Weather): Boolean {
        return oldItem.historyId != null
                && newItem.historyId != null
                && oldItem.historyId == newItem.historyId
    }

    override fun areContentsTheSame(oldItem: Weather, newItem: Weather): Boolean {
        return oldItem == newItem
    }
}