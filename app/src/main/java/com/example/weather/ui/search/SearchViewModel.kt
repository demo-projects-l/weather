package com.example.weather.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weather.ui.Event

class SearchViewModel : ViewModel() {
    private val _navigateToCity = MutableLiveData<Event<String>>()
    val navigateToCity : LiveData<Event<String>>
        get() = _navigateToCity

    private val _navigateToHistory = MutableLiveData<Event<Boolean>>()
    val navigateToHistory : LiveData<Event<Boolean>>
        get() = _navigateToHistory

    fun findCity(name: String) {
        _navigateToCity.value = Event(name)
    }

    fun goToHistory() {
        _navigateToHistory.value = Event(true)
    }
}