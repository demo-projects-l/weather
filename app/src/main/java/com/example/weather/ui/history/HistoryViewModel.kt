package com.example.weather.ui.history

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.weather.domain.model.Weather
import com.example.weather.domain.usecase.ObserveHistoryUseCase
import com.example.weather.domain.Result
import com.example.weather.domain.Result.*
import com.example.weather.domain.usecase.RefreshHistoryDataUseCase

class HistoryViewModel @ViewModelInject constructor(
    private val observeHistoryUseCase: ObserveHistoryUseCase,
    private val refreshHistoryDataUseCase: RefreshHistoryDataUseCase,
) : ViewModel() {
    private val _history: LiveData<Result<List<Weather>>> = liveData {
        emit(Loading)
        emitSource(observeHistoryUseCase())

        refreshHistoryDataUseCase()
    }

    val history = _history.map {
        if (it is Success) it.data else emptyList()
    }

    val dataLoading = _history.map {
        it is Loading
    }
}