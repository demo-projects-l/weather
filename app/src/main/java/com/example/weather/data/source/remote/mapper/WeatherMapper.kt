package com.example.weather.data.source.remote.mapper

import com.example.weather.data.source.remote.entity.WeatherEntity
import com.example.weather.domain.model.Weather
import java.time.Clock
import java.util.*
import javax.inject.Inject

class WeatherMapper @Inject constructor(private val clock: Clock) {
    fun mapFromEntity(entity: WeatherEntity): Weather {
        // TODO something with first
        return Weather(
            entity.city,
            entity.main.temperature,
            entity.weather.first().description,
            entity.weather.first().icon,
            entity.datetime,
            clock.instant(),
            entity.id,
        )
    }
}