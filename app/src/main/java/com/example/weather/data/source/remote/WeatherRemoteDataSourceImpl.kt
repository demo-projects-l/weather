package com.example.weather.data.source.remote

import com.example.weather.data.source.WeatherRemoteDataSource
import com.example.weather.data.source.remote.mapper.WeatherMapper
import com.example.weather.domain.Result.*
import com.example.weather.domain.Result
import com.example.weather.domain.exception.BadInputException
import com.example.weather.domain.exception.ExternalException
import com.example.weather.domain.exception.NetworkException
import com.example.weather.domain.exception.UnauthorizedException
import com.example.weather.domain.model.Weather
import retrofit2.HttpException
import java.net.UnknownHostException

class WeatherRemoteDataSourceImpl(
    private val serviceApi: WeatherApiService,
    private val dataMapper: WeatherMapper,
) : WeatherRemoteDataSource {
    override suspend fun getCityWeather(cityName: String): Result<Weather> {
        return withExceptionsHandled {
            dataMapper.mapFromEntity(serviceApi.getCityWeather(cityName))
        }
    }

    override suspend fun getForCities(ids: List<Long>): Result<List<Weather>> {
        return withExceptionsHandled {
            serviceApi.getForCities(ids.joinToString(","))
                .cities
                .map { dataMapper.mapFromEntity(it) }
        }
    }

    private suspend fun <T> withExceptionsHandled(block: suspend () -> T): Result<T> {
        return try {
            Success(block())
        } catch (e: HttpException) {
            // TODO log
            if (e.code() == 401) {
                return Error(UnauthorizedException(e.message()))
            }

            if (e.code() == 404) {
                return Error(BadInputException(e.message()))
            }

            if (e.code() >= 500) {
                return Error(ExternalException(e.message()))
            }

            return Error(RuntimeException(e.message()))
        } catch (e: UnknownHostException) {
            // TODO logging
            return Error(NetworkException(e.message))
        } catch (e: Throwable) {
            // TODO logging
            return Error(RuntimeException("Unrecognized error"))
        }
    }
}