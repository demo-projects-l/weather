package com.example.weather.data.source.remote

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant

class DateAdapter() {
    @ToJson
    fun toJson(instant: Instant): Long {
        return instant.epochSecond
    }

    @FromJson
    fun fromJson(timestampInSeconds: Long): Instant {
        return Instant.ofEpochSecond(timestampInSeconds)
    }
}