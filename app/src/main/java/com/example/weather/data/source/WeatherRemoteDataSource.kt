package com.example.weather.data.source

import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather

interface WeatherRemoteDataSource {
    suspend fun getCityWeather(cityName: String): Result<Weather>
    suspend fun getForCities(ids: List<Long>): Result<List<Weather>>
}