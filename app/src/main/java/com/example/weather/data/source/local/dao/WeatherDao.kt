package com.example.weather.data.source.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.weather.data.source.local.entity.WeatherEntity
import com.example.weather.domain.model.Weather

@Dao
interface WeatherDao {
    @Query("SELECT * FROM weather ORDER BY viewed_at DESC")
    fun observe() : LiveData<List<WeatherEntity>>

    @Insert
    suspend fun save(weather: WeatherEntity)

    @Update
    suspend fun update(weather: WeatherEntity)

    @Query("SELECT * FROM weather WHERE city_name = :name COLLATE NOCASE")
    suspend fun getCity(name: String): WeatherEntity?

    @Query("SELECT * FROM weather")
    suspend fun getAll(): List<WeatherEntity>
}