package com.example.weather.data.source.local

import androidx.room.TypeConverter
import java.time.Instant
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Instant? {
        return value?.let { Instant.ofEpochSecond(value) }
    }

    @TypeConverter
    fun toTimestamp(instant: Instant?): Long? {
        return instant?.epochSecond
    }
}