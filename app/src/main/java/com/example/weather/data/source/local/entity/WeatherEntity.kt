package com.example.weather.data.source.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull
import java.time.Instant
import java.util.*

@Entity(tableName = "weather")
data class WeatherEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @NotNull
    val id: Long = 0L,

    @ColumnInfo(name = "city_name")
    val city: String,

    @ColumnInfo(name = "temperature")
    val temperature: Double,

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "icon")
    val icon: String,

    @ColumnInfo(name = "measured_at")
    val measuredAt: Instant,

    @ColumnInfo(name = "viewed_at")
    val viewedAt: Instant,

    @ColumnInfo(name = "remote_id")
    val remoteId: Long
)