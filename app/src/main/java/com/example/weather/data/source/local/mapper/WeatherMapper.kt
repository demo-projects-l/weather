package com.example.weather.data.source.local.mapper

import com.example.weather.data.source.local.entity.WeatherEntity
import com.example.weather.domain.model.Weather
import javax.inject.Inject

class WeatherMapper @Inject constructor() {
    fun mapFromEntity(entity: WeatherEntity): Weather {
        return Weather(
            entity.city,
            entity.temperature,
            entity.description,
            entity.icon,
            entity.measuredAt,
            entity.viewedAt,
            entity.remoteId,
            entity.id
        )
    }

    fun mapToEntity(model: Weather, id: Long = 0L): WeatherEntity {
        return WeatherEntity(
            id = id,
            city = model.cityName,
            temperature = model.temperature,
            description = model.description,
            icon = model.icon,
            remoteId = model.remoteId,
            viewedAt = model.viewedAt,
            measuredAt = model.measuredAt,
        )
    }
}