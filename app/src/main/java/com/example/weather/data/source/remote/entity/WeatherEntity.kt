package com.example.weather.data.source.remote.entity

import com.squareup.moshi.Json
import java.time.Instant
import java.util.*

data class WeatherEntity(
    @Json(name = "name")
    val city: String,

    @Json(name = "main")
    val main: Main,

    @Json(name = "weather")
    val weather: List<Weather>,

    @Json(name = "dt")
    val datetime: Instant,

    @Json(name = "id")
    val id: Long,
)

data class WeatherListEntity(
    @Json(name = "list")
    val cities: List<WeatherEntity>
)

data class Weather(
    @Json(name = "description")
    val description: String,

    @Json(name = "icon")
    val icon: String,
)

data class Main(
    @Json(name = "temp")
    val temperature: Double,
)