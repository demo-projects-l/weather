package com.example.weather.data

import androidx.lifecycle.LiveData
import com.example.weather.data.source.HistoryLocalDataSource
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.HistoryRepository
import javax.inject.Inject

class DefaultHistoryRepository @Inject constructor(
    private val localDataSource: HistoryLocalDataSource
) : HistoryRepository {
    override suspend fun save(weather: Weather) {
        localDataSource.save(weather)
    }

    override suspend fun update(historyId: Long, weather: Weather) {
        localDataSource.update(historyId, weather)
    }

    override suspend fun observe(): LiveData<List<Weather>> {
        return localDataSource.observe()
    }

    override suspend fun getCity(name: String): Weather? {
        return localDataSource.getCity(name)
    }

    override suspend fun getAll(): List<Weather> {
        return localDataSource.getAll()
    }
}