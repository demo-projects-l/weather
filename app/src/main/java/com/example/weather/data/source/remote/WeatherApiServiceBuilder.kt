package com.example.weather.data.source.remote

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class WeatherApiServiceBuilder { // TODO rename openweathermapbuilder
    private lateinit var baseUrl: String
    private lateinit var apiKey: String
    private var measurementUnits: String? = null // TODO enum?

    fun baseUrl(url: String): WeatherApiServiceBuilder {
        baseUrl = url

        return this
    }

    fun apiKey(key: String): WeatherApiServiceBuilder {
        apiKey = key

        return this
    }

    fun measurementUnits(units: String): WeatherApiServiceBuilder {
        measurementUnits = units

        return this
    }

    fun build(): WeatherApiService {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(DateAdapter())
            .build()

        val client = OkHttpClient.Builder()
            .addInterceptor { chain ->
                var request = chain.request()
                val urlBuilder = request
                    .url()
                    .newBuilder()
                    .addQueryParameter("appid", apiKey)

                if (measurementUnits != null) {
                    urlBuilder.addQueryParameter("units", measurementUnits)
                }

                request = request.newBuilder().url(urlBuilder.build()).build()

                return@addInterceptor chain.proceed(request)
            }.build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(WeatherApiService::class.java)
    }
}