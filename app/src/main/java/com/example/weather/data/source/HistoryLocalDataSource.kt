package com.example.weather.data.source

import androidx.lifecycle.LiveData
import com.example.weather.domain.model.Weather

interface HistoryLocalDataSource {
    suspend fun save(city: Weather)
    suspend fun update(id: Long, city: Weather)
    suspend fun observe(): LiveData<List<Weather>>
    suspend fun getCity(name: String): Weather?
    suspend fun getAll(): List<Weather>
}