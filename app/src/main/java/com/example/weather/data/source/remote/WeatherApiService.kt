package com.example.weather.data.source.remote

import com.example.weather.data.source.remote.entity.WeatherEntity
import com.example.weather.data.source.remote.entity.WeatherListEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {
    @GET("weather")
    suspend fun getCityWeather(@Query("q") city: String): WeatherEntity

    @GET("group")
    suspend fun getForCities(@Query("id") ids: String): WeatherListEntity
}