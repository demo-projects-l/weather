package com.example.weather.data

import com.example.weather.data.source.HistoryLocalDataSource
import com.example.weather.data.source.WeatherRemoteDataSource
import com.example.weather.domain.Result
import com.example.weather.domain.model.Weather
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.domain.succeeded
import javax.inject.Inject

class DefaultWeatherRepository @Inject constructor(
    private val remoteDataSource: WeatherRemoteDataSource
) : WeatherRepository {
    override suspend fun getCityWeather(cityName: String): Result<Weather> {
        return remoteDataSource.getCityWeather(cityName)
    }

    override suspend fun getForCities(ids: List<Long>): Result<List<Weather>> {
        return remoteDataSource.getForCities(ids)
    }
}