package com.example.weather.data.source.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.example.weather.data.source.HistoryLocalDataSource
import com.example.weather.data.source.local.dao.WeatherDao
import com.example.weather.data.source.local.mapper.WeatherMapper
import com.example.weather.domain.model.Weather

class HistoryLocalDataSourceImpl(
    private val dao: WeatherDao,
    private val mapper: WeatherMapper
) : HistoryLocalDataSource {
    override suspend fun save(city: Weather) {
        dao.save(mapper.mapToEntity(city))
    }

    override suspend fun update(id: Long, city: Weather) {
        dao.update(mapper.mapToEntity(city, id))
    }

    override suspend fun observe(): LiveData<List<Weather>> {
        return dao.observe().map { it.map { weather -> mapper.mapFromEntity(weather) } }
    }

    override suspend fun getCity(name: String): Weather? {
        return dao.getCity(name)?.let {
            mapper.mapFromEntity(it)
        }
    }

    override suspend fun getAll(): List<Weather> {
        return dao.getAll().map { mapper.mapFromEntity(it) }
    }
}