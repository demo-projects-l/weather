package com.example.weather.di

import android.content.Context
import com.example.weather.data.DefaultHistoryRepository
import com.example.weather.data.DefaultWeatherRepository
import com.example.weather.data.source.HistoryLocalDataSource
import com.example.weather.data.source.WeatherRemoteDataSource
import com.example.weather.data.source.local.WeatherDatabase
import com.example.weather.data.source.local.HistoryLocalDataSourceImpl
import com.example.weather.data.source.remote.WeatherApiServiceBuilder
import com.example.weather.data.source.remote.WeatherRemoteDataSourceImpl
import com.example.weather.data.source.remote.mapper.WeatherMapper
import com.example.weather.domain.repository.HistoryRepository
import com.example.weather.domain.repository.WeatherRepository
import com.example.weather.util.AndroidErrorTranslator
import com.example.weather.ui.ErrorTranslator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import java.time.Clock
import javax.inject.Singleton
import com.example.weather.data.source.local.mapper.WeatherMapper as LocalMapper

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideWeatherRepository(
        remoteDataSource: WeatherRemoteDataSource
    ): WeatherRepository {
        return DefaultWeatherRepository(remoteDataSource)
    }

    @Provides
    @Singleton
    fun provideHistoryRepository(
        localDataSource: HistoryLocalDataSource,
    ): HistoryRepository {
        return DefaultHistoryRepository(localDataSource)
    }

    @Provides
    @Singleton
    fun provideWeatherRemoteDataSource(): WeatherRemoteDataSource {
        val serviceApi = WeatherApiServiceBuilder()
            .baseUrl("https://api.openweathermap.org/data/2.5/") // TODO user config store
            .apiKey("7587eaff3affbf8e56a81da4d6c51d06")
            .measurementUnits("metric")
            .build()

        return WeatherRemoteDataSourceImpl(serviceApi, WeatherMapper(Clock.systemUTC()))
    }

    @Provides
    @Singleton
    fun provideWeatherLocalDataSource(
        @ApplicationContext appContext: Context
    ): HistoryLocalDataSource {
        return HistoryLocalDataSourceImpl(
            WeatherDatabase.getInstance(appContext).weatherDao(),
            LocalMapper()
        )
    }

    @Provides
    @Singleton
    fun provideErrorTranslator(
        @ApplicationContext appContext: Context
    ): ErrorTranslator {
        return AndroidErrorTranslator(appContext.resources)
    }
}