package com.example.weather.util

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import java.text.DecimalFormat
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.*

@BindingAdapter(
    "glideSrc",
    "width",
    "height",
    requireAll = false
)
fun ImageView.bindGlideSrc(
    src: String?,
    width: Int = 64,
    height: Int = 64,
) {
    src?.let {
        Glide
            .with(context)
            .load("https://openweathermap.org/img/wn/${src}@2x.png")
            .override(width, height) // TODO
            .into(this)
    }
}

@BindingAdapter("measuredAt")
@SuppressLint("SimpleDateFormat")
fun TextView.bindMeasuredAt(instant: Instant?) {
    instant?.let {
        this.text = DateTimeFormatter.ofPattern("EEE\ndd").withZone(ZoneOffset.UTC).format(instant)
    }
}

@BindingAdapter("temperature")
fun TextView.bindTemperature(temp: Double?) {
    temp?.let {
        val decimalFormat = DecimalFormat() // TODO move

        this.text = "${decimalFormat.format(temp)}°" // TODO
    }
}

@BindingAdapter("description")
fun TextView.bindDescription(description: String?) {
    description?.let {
        this.text = description.capitalize(Locale.getDefault())
    }
}

@BindingAdapter("city")
fun TextView.bindCity(city: String?) {
    city?.let {
        this.text = city
            .split(" ")
            .joinToString(" ") {
                it.capitalize(Locale.getDefault())
            }.trimEnd()
    }
}