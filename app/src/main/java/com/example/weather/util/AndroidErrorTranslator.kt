package com.example.weather.util

import android.content.res.Resources
import com.example.weather.R
import com.example.weather.domain.exception.BadInputException
import com.example.weather.domain.exception.ExternalException
import com.example.weather.domain.exception.NetworkException
import com.example.weather.domain.exception.UnauthorizedException
import com.example.weather.ui.ErrorTranslator
import javax.inject.Inject

class AndroidErrorTranslator @Inject constructor(
    private val resources: Resources
) : ErrorTranslator {
    override fun translate(exception: Exception): String {
        return when (exception) {
            is UnauthorizedException -> resources.getString(R.string.token_error)
            is BadInputException -> resources.getString(R.string.city_input_error)
            is ExternalException -> resources.getString(R.string.server_error)
            is NetworkException -> resources.getString(R.string.network_error)
            is RuntimeException -> resources.getString(R.string.unknown_error)
            else -> resources.getString(R.string.unknown_error)
        }
    }
}